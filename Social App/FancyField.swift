//
//  FancyField.swift
//  Social App
//
//  Created by Edward R on 3/9/17.
//  Copyright © 2017 Edward R. All rights reserved.
//

import UIKit
@IBDesignable
class FancyField: UITextField {
    private var materialKey = false
    
    @IBInspectable var materialDesign: Bool  {
        get {
            return materialKey
        } set {
            materialKey = newValue
            if materialKey {
                setupView()
            } else {
                self.layer.cornerRadius = 0
                self.layer.shadowOpacity = 0
                self.layer.shadowRadius = 0
                self.layer.shadowColor = nil
            }
        }
        
    }
    
    override func prepareForInterfaceBuilder() {
        setupView()
    }
   
    
    func setupView() {
        
        layer.borderColor = UIColor(red: SHADOW_GRAY, green: SHADOW_GRAY, blue: SHADOW_GRAY, alpha: 0.0).cgColor
        layer.borderWidth = 1.0
        layer.cornerRadius = 2.0
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 10, dy: 5)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 10, dy: 5)
    }
   
}

