//
//  CircleView.swift
//  Social App
//
//  Created by Edward R on 3/9/17.
//  Copyright © 2017 Edward R. All rights reserved.
//

import UIKit
@IBDesignable
class CircleView: UIImageView {

    private var materialKey = false
    @IBInspectable var circleView: Bool  {
        get {
            return materialKey
        } set {
            materialKey = newValue
            if materialKey {
                setupView()
            } 
        }
        
    }
    
//    override func draw(_ rect: CGRect) {
//        super.draw(rect)
//        setupView()
//    }
//    
    //
    //    override func layoutSubviews() {
    //        super.layoutSubviews()
    //        setupView()
    //    }
    override func prepareForInterfaceBuilder() {
        if materialKey {
          setupView()
        }
    }

    func setupView() {
        self.layer.cornerRadius = self.frame.width / 2
        self.layer.shadowOpacity = 0.8
        self.layer.shadowRadius = 3.0
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.layer.shadowColor = UIColor(red: 157/255, green: 157/255, blue: 157/255, alpha: 1.0).cgColor
        clipsToBounds = true
        
    }

}
