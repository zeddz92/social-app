//
//  Post.swift
//  Social App
//
//  Created by Edward R on 3/11/17.
//  Copyright © 2017 Edward R. All rights reserved.
//

import Foundation

class Post {
    private var _postKey: String!
    private var _likes: Int!
    private var _caption: String!
    private var _imageUrl: String!
    
    var postKey: String {
        return _postKey
    }
    
    var likes: Int {
        return _likes
    }
    
    var caption: String {
        return _caption
    }
    
    var imageUrl: String {
        return _imageUrl
    }
    
    init(postKey: String, postData: Dictionary<String, Any>) {
        self._postKey = postKey
        
        if let caption = postData["caption"] as? String {
            self._caption = caption
        }
        
        if let imageUrl = postData["imageUrl"] as? String {
            self._imageUrl = imageUrl
        }
        
        if let likes = postData["likes"] as? Int {
            self._likes = likes
        }
    }
    
    
}
