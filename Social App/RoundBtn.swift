//
//  RoundBtn.swift
//  Social App
//
//  Created by Edward R on 3/9/17.
//  Copyright © 2017 Edward R. All rights reserved.
//

import UIKit
@IBDesignable
class RoundBtn: UIButton {
    private var materialKey = false
    @IBInspectable var cornerRadius: Bool  {
        get {
            return materialKey
        } set {
            materialKey = newValue
            if materialKey {
                setupView()
            } else {
                self.layer.cornerRadius = 0
                self.layer.shadowOpacity = 0
                self.layer.shadowRadius = 0
                self.layer.shadowColor = nil
            }
        }

    }
    
    override func prepareForInterfaceBuilder() {
        setupView()
    }
    
    func setupView() {
        layer.cornerRadius = self.frame.width / 2
        // layer.shadowColor = UIColor(red: SHADOW_GRAY, green: SHADOW_GRAY, blue: SHADOW_GRAY, alpha: 0.0).cgColor
        // layer.shadowOpacity = 0.8
        // layer.shadowRadius = 5.0
        // layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        imageView?.contentMode = .scaleAspectFit
        
        self.layer.shadowOpacity = 0.8
        self.layer.shadowRadius = 3.0
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.layer.shadowColor = UIColor(red: 157/255, green: 157/255, blue: 157/255, alpha: 1.0).cgColor

    }

}
