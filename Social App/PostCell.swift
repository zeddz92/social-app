//
//  PostCell.swift
//  Social App
//
//  Created by Edward R on 3/10/17.
//  Copyright © 2017 Edward R. All rights reserved.
//

import UIKit
import Firebase

class PostCell: UITableViewCell {

    @IBOutlet weak var profileImg: CircleView!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var postImg: UIImageView!
    @IBOutlet weak var caption: UITextView!
    @IBOutlet weak var likesLbl: UILabel!
    @IBOutlet weak var activitySpinner: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(post: Post, img: UIImage? = nil) {
        caption.text = post.caption
        likesLbl.text = "\(post.likes)"
        activitySpinner.startAnimating()
        
        
        if(img != nil) {
            self.postImg.image = img
            activitySpinner.stopAnimating()
        } else {
            let url = URL(string: post.imageUrl)!
            let ref = FIRStorage.storage().reference(forURL: post.imageUrl)
            ref.data(withMaxSize: 2 * 1024 *  1024, completion: { (data, error) in
                if error != nil {
                    print("unable to download image from fire storage")
                } else {
                    
                    if let imageData = data {
                        
                        if let img = UIImage(data: imageData) {
                            self.postImg.image = img
                            self.activitySpinner.stopAnimating()
                            FeedVC.imageCache.setObject(img, forKey: post.imageUrl as NSString)
                        }
                    }
                }
            })
            
            
        }
        
   

        // usernameLbl.text = post
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
